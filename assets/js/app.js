$(document).ready(function () {
    $('.md-slide-home').slick({
        dots: true,
        infinite: true,
        speed: 1200,
        arrows: false,
        autoplay: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    infinite: true,
                    dots: true
                }
            }
        ]
    });

    $('.jsSearch').click(function () {
        $('.md-form').fadeIn();
        $('.md-form .md-inserach').focus();
        $('main').addClass('md-blur');
    })

    $('.jsCloseForm').click(function () {
        $('.md-form').fadeOut();
        $('main').removeClass('md-blur');
    })
})